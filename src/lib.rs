//    libeemo, a library for eemo servers and clients to use for communication
//    Copyright (C) 2017  Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[macro_use]
extern crate serde_derive;
//extern crate bincode;
extern crate rmp_serde as serialize;
extern crate serde;

use serde::{ser,de};

pub mod decode {
    pub use serialize::decode::Error;
}
pub mod encode {
    pub use serialize::encode::Error;
}

//use bincode::Bounded;

use std::collections::HashMap;
use std::io::{Write,Cursor};
use std::ops::Add;

#[derive(Eq,PartialEq,Hash,Clone,Copy,Serialize,Deserialize,Debug)]
pub struct Position(pub i64,pub i64);

impl Add<Position> for Position {
    type Output = Position;
    fn add(self, other: Position) -> Position {
        Position(self.0 + other.0, self.1 + other.1)
    }

}
impl Add<Direction> for Position {
    type Output = Position;
    fn add(self, other: Direction) -> Position {
        match other {
            Direction::North => Position(self.0,self.1+1),
            Direction::East => Position(self.0+1,self.1),
            Direction::South => Position(self.0,self.1-1),
            Direction::West => Position(self.0-1,self.1)
        }
    }
}

#[derive(Serialize,Deserialize,Debug,PartialEq,Clone)]
pub enum AuthError {
    BadPassword,
    //This error shouldn't happen if there are no bugs
    InvalidUsername,
    BadInput,
    MultipleUsers,
    DBError,
    EnvError,
    IOError,
    UserNotFound,
    UsernameReserved
}

#[derive(Serialize,Deserialize,Debug,Clone,PartialEq)]
pub struct SendWorld {
    pub players: HashMap<String,Position>,
    pub objects: HashMap<Position, Object>,
}
#[derive(Serialize,Deserialize,Clone,PartialEq,Debug)]
pub enum ClientToServerMessage {
    Auth(String,String), //User, Pass
    Register(String,String), //User, Pass
    Move(Position),
    ObjectAdd(Object,Position), //Gate name 
    ObjectDel(Position),
    Activate(Position,u32), //Send data to object at position
    ChatMessage(String),
    GetWorld,
    GetWorldSubset(Position,Position), //Get a box from first to second position
    GetPosition,
    GetSource,
    GetName,
}
impl ClientToServerMessage {
    pub fn from_vec(reader: &mut Vec<u8>) -> Result<ClientToServerMessage,serialize::decode::Error> {
        from_vec(reader)
    }
    pub fn write_out<W: Write>(&self, writer: &mut W) -> Result<(),serialize::encode::Error>{
        write_out(&self,writer)
    }
}
#[derive(Serialize,Deserialize,Debug,Clone,PartialEq)]
pub enum ServerToClientMessage {
    Game(SendWorld),
    YourPosition(Position),
    AuthResponse(Result<(),AuthError>), 
    ChatMessage(String,String), //Message, username
    //Instructions to get source code,
    //source code itself,
    //or notice that source is not available if
    //you've implemented your own server from scratch
    Source(String), 
    //Name of player
    Name(String)
}
impl ServerToClientMessage {
    pub fn write_out<W: Write>(&self, writer: &mut W) -> Result<(),serialize::encode::Error>{
        write_out(&self,writer)
    }
    pub fn from_vec(reader: &mut Vec<u8>) -> Result<ServerToClientMessage,serialize::decode::Error>{
        from_vec(reader)
    }
}
#[derive(Serialize,Deserialize,Debug,Clone,PartialEq)]
pub struct Object {
    pub object_data: ObjectData,
    pub orientation: Direction
}
impl Object {
    pub fn get_data(&self) -> ObjectData {
        self.object_data
    }
    pub fn get_orientation(&self) -> Direction {
        self.orientation
    }
}
#[derive(Serialize,Deserialize,Debug,Clone,Copy,PartialEq)]
pub enum Direction {
    North,
    South,
    East,
    West
}
impl Add for Direction {
    type Output = Direction;
    
    fn add(self, other: Direction) -> Direction {
        match other {
            Direction::North => self,
            Direction::South => match self {
                Direction::North=> Direction::South,
                Direction::East => Direction::West,
                Direction::South => Direction::North,
                Direction::West => Direction::East,
            },
            Direction::East => match self {
                Direction::North=> Direction::East,
                Direction::East => Direction::South,
                Direction::South => Direction::West,
                Direction::West => Direction::North,
            },
            Direction::West => match self {
                Direction::North=> Direction::West,
                Direction::East => Direction::North,
                Direction::South => Direction::East,
                Direction::West => Direction::South,
            }
        }
    }
    
}
#[derive(Serialize,Deserialize,Debug,Clone,Copy,PartialEq)]
pub enum ObjectData {
    Gate(Gate),
    Light(bool), //On or off
    Lever(bool), //On or off
}

#[derive(Serialize,Deserialize,PartialEq,Clone,Copy,Eq,Debug)]
pub enum Gate {
    And,
    Or,
    Xor
}



fn write_out<W: Write,V: ser::Serialize>(value: &V, writer: &mut W) -> Result<(),serialize::encode::Error>{
    let mut ser = serialize::Serializer::new(writer);
    try!(value.serialize(&mut ser));
    Ok(())
}
fn from_vec<'a,D: de::Deserialize<'a>>(reader: &mut Vec<u8>) -> Result<D,serialize::decode::Error>{
    //TODO: there's probably a way to get rid of this clone
    let vec_to_read = reader.clone();
    let mut deserializer = serialize::Deserializer::from_read(Cursor::new(vec_to_read.as_slice()));
    let result = de::Deserialize::deserialize(&mut deserializer);
    if result.is_ok() {
        let bytes_read = deserializer.position();
        let new_reader = reader.split_off(bytes_read as usize);
        *reader = new_reader;
    }
    result
    
}



#[cfg(test)]
mod test {
    //use super::{ServerToClientMessage,ClientToServerMessage};
    use super::{ClientToServerMessage,Direction,Position};
    #[test]
    fn single_serialize_test() {
        let mut buffer = Vec::new();
        ClientToServerMessage::GetPosition.write_out(&mut buffer).expect("Failed to write into buffer");
        assert_eq!(
            ClientToServerMessage::GetPosition,
            ClientToServerMessage::from_vec(&mut buffer).unwrap()
            );
    }
    #[test]
    fn double_serialize_test() {
        let mut buffer = Vec::new();
        ClientToServerMessage::GetPosition.write_out(&mut buffer).expect("Failed to write into buffer");
        ClientToServerMessage::ChatMessage("test".to_owned()).write_out(&mut buffer).expect("Failed to write into buffer");
        assert_eq!(
            ClientToServerMessage::GetPosition,
            ClientToServerMessage::from_vec(&mut buffer).unwrap()
            );
        assert_eq!(
            ClientToServerMessage::ChatMessage("test".to_owned()),
            ClientToServerMessage::from_vec(&mut buffer).unwrap()
            );
    }
    #[test]
    fn direction_add_test() {
        assert_eq!(Direction::South + Direction::South, Direction::North);
        assert_eq!(Direction::East + Direction::West, Direction::North);
        assert_eq!(Direction::West + Direction::South, Direction::East);
        assert_eq!(Direction::North + Direction::North, Direction::North);
    }
    #[test]
    fn position_add_test() {
        assert_eq!(Position(0,0) + Direction::North,Position(0,1));
        assert_eq!(Position(0,0) + Direction::South,Position(0,-1));
        assert_eq!(Position(0,0) + Direction::East,Position(1,0));
        assert_eq!(Position(0,0) + Direction::West,Position(-1,0));
    }
    #[test]
    fn position_position_add_test() {
        assert_eq!(Position(0,0) + Position(3,4),Position(3,4));
        assert_eq!(Position(2,2) + Position(3,-4),Position(5,-2));
        assert_eq!(Position(1,-1) + Position(-1,1),Position(0,0));

    }
}
